<?php

class Ship
{
    private int $x = 0;
    private int $y = 0;
    private int $speed = 0;

    public function __construct()
    {
        $this->print_location('ready for launch');
    }

    public function speed_up()
    {
        if ($this->speed === 5) {
            $this->update_location('maximum speed');
            return;
        }

        $this->speed++;
        $this->update_location();
    }

    public function slow_down()
    {
        if ($this->speed === 0) {
            return;
        }

        // if the ship is launched, its speed cannot go under 1
        if ($this->y > 0 and $this->speed === 1) {
            $this->update_location('minimum speed');
            return;
        }

        $this->speed--;
        $this->update_location();
    }

    public function move_right()
    {
        $this->x++;

        if ($this->x > 5) {
            $this->update_location('wrong trajectory');
            return;
        }

        $this->update_location();
    }

    public function move_left()
    {
        $this->x--;

        if ($this->x < -5) {
            $this->update_location('wrong trajectory');
            return;
        }

        $this->update_location();
    }

    private function update_location(string $append = '')
    {
        $this->y += $this->speed;

        if ($this->y === 250 and $this->x === 0) {
            $this->print_location('on the moon');
            return;
        }

        if ($this->y > 250) {
            $this->print_location('contact lost');
            return;
        }

        $this->print_location($append);
    }

    public function is_done(): bool
    {
        return $this->on_moon() or $this->contact_lost();
    }

    private function on_moon(): bool
    {
        return $this->y === 250 and $this->x === 0;
    }

    private function contact_lost(): bool
    {
        return $this->y > 250;
    }

    public function print_location(string $append = '')
    {
        echo "($this->x, $this->y) $append" . PHP_EOL;
    }
}

class Game
{
    private Ship $ship;

    public function __construct()
    {
        $this->ship = new Ship();
    }

    public function run()
    {
        while (!$this->ship->is_done()) {
            $input = readline();
            $this->handle_command($input);
        }
    }

    private function handle_command(string $input)
    {
        switch ($input) {
            case 'w':
                $this->ship->speed_up();
                break;
            case 's':
                $this->ship->slow_down();
                break;
            case 'a':
                $this->ship->move_left();
                break;
            case 'd':
                $this->ship->move_right();
                break;
            default:
                echo 'Invalid command.' . PHP_EOL;
                break;
        }
    }
}

$game = new Game();
$game->run();
